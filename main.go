package main

import (
	"fmt"
	"log"
	"tuangnoi/connection"
	"tuangnoi/myconfig/mysetup"
	"tuangnoi/myconfig/myvar"
	"tuangnoi/myconfig/server"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load(".env")
	myvar.SetEnv()

	tuangnoiDB := connection.NewDb(myvar.ENV_DB_PROJECT)
	tuangnoiRedis := connection.NewRedis(myvar.ENV_REDIS_ADDR)

	db, _ := tuangnoiDB.DB()
	defer db.Close()
	defer tuangnoiRedis.Close()

	r := server.New()
	server.GetRoutes(r, tuangnoiDB, tuangnoiRedis)
	mysetup.SetUpDB()

	log.Fatal(r.Listen(fmt.Sprintf(":%s", myvar.ENV_SERVICE_PORT)))
}
