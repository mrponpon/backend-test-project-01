package connection

import (
	"os"
	"time"

	"github.com/go-redis/redis"
)

var REDIS *redis.Client

func NewRedis(params ...string) *redis.Client {
	REDIS = redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS_ADDR"),
		DB:   0,
	})
	err := REDIS.Set("key", "value", time.Second*5).Err()
	if err != nil {
		panic(err)
	}

	_, err = REDIS.Get("key").Result()
	if err != nil {
		panic(err)
	}
	return REDIS
}

func GetRedisInstance() *redis.Client {
	return REDIS
}
