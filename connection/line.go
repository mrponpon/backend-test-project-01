package connection

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type lineMessageType string

const (
	TYPE_MESSAGE lineMessageType = "text"
	TYPE_FLEX    lineMessageType = "flex"
)

type (
	LineBodySendMessage struct {
		To      string            `json:"to"`
		Message []LineBodyMessage `json:"message"`
	}
	LineBodyMessage struct {
		Type string `json:"type"`
		Text string `json:"text"`
	}
)

type (
	LineBodySendBubble struct {
		To      string           `json:"to"`
		Message []LineBodyBubble `json:"message"`
	}
	LineBodyBubble struct {
		Type    string                 `json:"type"`
		Content map[string]interface{} `json:"contents"`
	}
)

func LineSendMessage(lineToken string, msg interface{}) error {
	url := "https://api.line.me/v2/bot/message/push"

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(msg)

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPost, url, payloadBuf)

	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", lineToken))

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	_, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	return nil
}
