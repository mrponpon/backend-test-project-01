package connection

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

type BodyUser struct {
	Email string `json:"email"`
	UUID  string `json:"uuid"`
}

func Authorization() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			authHeader := strings.Split(c.Request().Header.Get("Authorization"), " ")
			if len(authHeader) < 2 {
				return next(c)
			}
			tokenString := authHeader[1]
			token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})
			if err != nil {
				return next(c)
			}

			claims, ok := token.Claims.(jwt.MapClaims)
			if ok {
				sub, ok := claims["sub"].(string)

				if ok {
					c.Request().Header.Add("USERID", sub)
				}
			}

			return next(c)
		}
	}
}

func AuthorizationFiber() fiber.Handler {
	return func(c *fiber.Ctx) error {
		authHeader := strings.Split(c.Get("Authorization"), " ")
		if len(authHeader) < 2 {
			return c.Next()
		}
		tokenString := authHeader[1]
		token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})
		if err != nil {
			return c.Next()
		}

		claims, ok := token.Claims.(jwt.MapClaims)

		if ok {
			sub, ok := claims["sub"].(string)
			if ok {
				c.Request().Header.Add("USERID", sub)
			}

			user, ok := claims["user"].(map[string]interface{})
			if ok {
				c.Request().Header.Add("UUID", fmt.Sprint(user["uuid"]))
			}
		}

		return c.Next()
	}
}
