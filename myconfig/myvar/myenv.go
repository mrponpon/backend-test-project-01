package myvar

import (
	"os"

	zlog "github.com/rs/zerolog/log"
)

var ENV_SERVICE_PORT string
var ENV_DB_PROJECT string
var ENV_REDIS_ADDR string

var ENV_LINE_ACCESS_TOKEN string

func GetMyENV(EnvKey string, defaultValue ...string) string {

	value := os.Getenv(EnvKey)
	zlog.Info().Msgf("[ENV] %s: %s", EnvKey, value)

	if len(value) == 0 && len(defaultValue) != 0 {
		value = defaultValue[0]
	} else if len(value) == 0 && len(defaultValue) == 0 {
		value = ""
	}
	return value
}

func SetEnv() {
	ENV_SERVICE_PORT = GetMyENV("PORT")
	ENV_DB_PROJECT = GetMyENV("DB_PROJECT")
	ENV_REDIS_ADDR = GetMyENV("REDIS_ADDR")
	ENV_LINE_ACCESS_TOKEN = GetMyENV("LINE_ACCESS_TOKEN")
}
