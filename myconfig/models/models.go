package models

import "github.com/shopspring/decimal"

type TypeBill string
type StatusPersonPaid string

const (
	TypeEqual  TypeBill = "EQUAL"
	TypeByUser TypeBill = "BY_USER"
)

const (
	StatusPaid   StatusPersonPaid = "PAIDED"
	StatusUnPaid StatusPersonPaid = "UNPAIDED"
)

type (
	BodyLineMessage struct {
		Target  string `json:"target"`
		Message string `json:"message"`
	}

	BodyLineBubble struct {
		Target  string                 `json:"target"`
		Content map[string]interface{} `json:"content"`
	}
)

type (
	ResponseBill struct {
		ID       int               `json:"id"`
		BillName string            `json:"bill_name"`
		TypePaid string            `json:"type_paid"`
		Summary  decimal.Decimal   `json:"summary"`
		Person   []DBBillingPerson `json:"person"`
		Items    []ResBillItems    `json:"items"`
	}

	ResBillItems struct {
		ID       int                 `json:"id"`
		ItemName string              `json:"item_name"`
		Price    decimal.NullDecimal `json:"price"`
		Person   []ResItemPerson     `json:"person"`
	}

	ResItemPerson struct {
		ID   int    `json:"id"`
		UUID string `json:"uuid"`
		Name string `json:"name"`
	}
)

type (
	DBBilling struct {
		ID       int                 `json:"id"`
		Name     string              `json:"name"`
		TypePaid string              `json:"type_paid"`
		Summary  decimal.NullDecimal `json:"summary"`
	}

	BodyBillingCreate struct {
		Name  string `json:"name"`
		Items []struct {
			ItemName string              `json:"item_name"`
			Price    decimal.NullDecimal `json:"price"`
		} `json:"items"`
	}
)

type (
	DBItems struct {
		ID     int                 `json:"id"`
		Name   string              `json:"name"`
		BillID int                 `json:"bill_id"`
		Person string              `json:"person"`
		Price  decimal.NullDecimal `json:"price"`
	}
	BodyItemAddToBill struct {
		BillID int `json:"bill_id"`
		Items  []struct {
			ItemName string              `json:"item_name"`
			Price    decimal.NullDecimal `json:"price"`
		} `json:"items"`
	}
	BodyItemUpdate struct {
		Items []struct {
			ID       int                 `json:"id"`
			ItemName string              `json:"item_name"`
			Price    decimal.NullDecimal `json:"price"`
		} `json:"items"`
	}
	BodyPersonAddToBill struct {
		BillID int `json:"bill_id"`
		Person []struct {
			UUID    string          `json:"person_uuid"`
			Name    string          `json:"person_name"`
			Summary decimal.Decimal `json:"summary"`
		}
	}

	BodyBillingPersonUpdate struct {
		ID      int             `json:"id"`
		Status  string          `json:"status"`
		UUID    string          `json:"person_uuid"`
		Name    string          `json:"person_name"`
		Summary decimal.Decimal `json:"summary"`
	}
)

type (
	DBBillingPerson struct {
		ID      int             `json:"id"`
		Status  string          `json:"status"`
		BillID  int             `json:"bill_id"`
		UUID    string          `json:"uuid"`
		Name    string          `json:"name"`
		Summary decimal.Decimal `json:"summry"`
	}
	BodyPersonAddToItem struct {
		ItemID int `json:"item_id"`
		Person []struct {
			PersonUUID string `json:"person_uuid"`
		}
	}
)

func (db DBBilling) TableName() string {
	return "billing"
}

func (db DBBillingPerson) TableName() string {
	return "billing_person"
}

func (db DBItems) TableName() string {
	return "billing_detail"
}
