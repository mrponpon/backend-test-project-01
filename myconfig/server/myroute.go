package server

import (
	"tuangnoi/apps/handlers"
	"tuangnoi/apps/repositories"
	"tuangnoi/apps/services"

	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

func GetRoutes(f *fiber.App, DB *gorm.DB, Redis *redis.Client) {

	repo := repositories.NewRepo(DB)
	ser := services.NewService(repo, Redis)
	h := handlers.NewHandler(ser)

	gAPI := f.Group("/api")
	gAPI.Post("/callback", h.WebhookCalback)
	gAPI.Post("/line/bot/message", h.PostLineMessage)
	gAPI.Post("/line/bot/bubble", h.PostLineBubble)

	gAPI.Get("/billing", h.BillingGetByID)
	gAPI.Get("/billing/person", h.BillingGetByPersonUUID)
	gAPI.Post("/billing", h.BillingCreate)
	gAPI.Put("/billing", h.BillingUpdate)

	gAPI.Get("/items", h.ItemGetByBillID)
	gAPI.Post("/items", h.ItemAddToBill)
	gAPI.Put("/items", h.ItemUpdate)

	gAPI.Put("/person/billing", h.PersonBillUpdate)
	gAPI.Post("/person/billing", h.PersonAddToBill)
	gAPI.Post("/person/items", h.PersonAddToItem)
	gAPI.Delete("/person/:person_id", h.PersonRemove)

}
