package mysetup

import (
	"tuangnoi/connection"
	"tuangnoi/myconfig/models"
	"tuangnoi/utils/helpers"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

func SetUpDB() {
	mydb := connection.GetDbInstance()
	SetUpDBBilling(mydb)
	SetUpDBItems(mydb)
	SetUpDBBillingPerson(mydb)
}

func SetUpDBBilling(mydb *gorm.DB) {
	err := mydb.AutoMigrate(&models.DBBilling{})
	if err != nil {
		log.Fatal().Msg(helpers.MyErrFormat(err).Error())
		return
	}
	log.Info().Msgf("[%s] success", helpers.MyFuncName())
}

func SetUpDBItems(mydb *gorm.DB) {
	err := mydb.AutoMigrate(&models.DBItems{})
	if err != nil {
		log.Fatal().Msg(helpers.MyErrFormat(err).Error())
		return
	}
	log.Info().Msgf("[%s] success", helpers.MyFuncName())
}

func SetUpDBBillingPerson(mydb *gorm.DB) {
	err := mydb.AutoMigrate(&models.DBBillingPerson{})
	if err != nil {
		log.Fatal().Msg(helpers.MyErrFormat(err).Error())
		return
	}
	log.Info().Msgf("[%s] success", helpers.MyFuncName())
}
