package services

import (
	"context"
	"tuangnoi/apps/repositories"
	"tuangnoi/myconfig/models"

	"github.com/go-redis/redis"
)

type sService struct {
	r     repositories.Repositories
	Redis *redis.Client
}

func NewService(r repositories.Repositories, Redis *redis.Client) Services {
	return &sService{
		r:     r,
		Redis: Redis,
	}
}

type Services interface {
	SendLineMessage(ctx context.Context, toTarget string, msg string) error
	SendLineBubble(ctx context.Context, toTarget string, content map[string]interface{}) error

	BillingGetByID(ctx context.Context, billID int) (interface{}, error)
	BillingGetByPersonUUID(ctx context.Context, personUUID string) (interface{}, error)
	BillingCreate(ctx context.Context, myinput models.BodyBillingCreate) error
	BillingUpdate(ctx context.Context, myinput models.DBBilling) error

	ItemGetByBillID(ctx context.Context, billID int) (interface{}, error)
	ItemUpdate(ctx context.Context, myinput models.BodyItemUpdate) error
	ItemAddToBill(ctx context.Context, myinput models.BodyItemAddToBill) error

	PersonAddToBill(ctx context.Context, myinput models.BodyPersonAddToBill) error
	PersonAddToItem(ctx context.Context, myinput models.BodyPersonAddToItem) error
	PersonBillUpdate(ctx context.Context, myinput models.BodyBillingPersonUpdate) error
	PersonRemove(ctx context.Context, personID int) error
}
