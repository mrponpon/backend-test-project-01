package services

import (
	"context"
	"strings"
	"tuangnoi/myconfig/models"

	"github.com/shopspring/decimal"
)

func (s *sService) BillingGetByPersonUUID(ctx context.Context, personUUID string) (interface{}, error) {
	mydata, err := s.r.BillingGetByPersonUUID(ctx, personUUID)
	return mydata, err
}

func (s *sService) BillingGetByID(ctx context.Context, billID int) (interface{}, error) {
	dBill, err := s.r.BillingGetByBillID(ctx, billID)
	if err != nil {
		return nil, err
	}

	dItem, err := s.r.ItemGetByBillID(ctx, dBill.ID)
	if err != nil {
		return nil, err
	}

	dPerson, err := s.r.PersonGetByBillID(ctx, dBill.ID)
	if err != nil {
		return nil, err
	}

	res := billingResponse(dPerson, dItem)

	return res, err
}

func (s *sService) BillingCreate(ctx context.Context, myinput models.BodyBillingCreate) error {

	var summary decimal.Decimal
	for _, i := range myinput.Items {
		summary = summary.Add(i.Price.Decimal)
	}

	insertBill, err := s.r.BillingCreate(ctx, models.DBBilling{
		Name:    myinput.Name,
		Summary: decimal.NewNullDecimal(summary),
	})
	if err != nil {
		return err
	}

	var insertItem []models.DBItems
	for _, item := range myinput.Items {
		i := models.DBItems{
			Name:   item.ItemName,
			BillID: insertBill.ID,
			Price:  item.Price,
		}
		insertItem = append(insertItem, i)
	}
	return s.r.ItemAddToBill(ctx, insertItem)
}

func (s *sService) BillingUpdate(ctx context.Context, myinput models.DBBilling) error {
	return s.r.BillingUpdate(ctx, myinput)
}

func billingResponse(dPerson []models.DBBillingPerson, dItem []models.DBItems) models.ResponseBill {
	var resBill = models.ResponseBill{}
	var resItem = []models.ResBillItems{}
	var resPerson = []models.DBBillingPerson{}

	var mapPerson = map[string]models.DBBillingPerson{}
	for _, p := range dPerson {
		mapPerson[p.UUID] = p
	}

	var mapSummaryPerson = map[string]decimal.Decimal{}
	var allSummary decimal.Decimal
	for _, item := range dItem {
		itemPerson := strings.Split(item.Person, ",")

		var myItem models.ResBillItems
		for _, uuid := range itemPerson {
			dper := mapPerson[uuid]
			mapSummaryPerson[uuid] = mapSummaryPerson[uuid].Add(item.Price.Decimal)
			myItem.Person = append(myItem.Person, models.ResItemPerson{
				ID:   dper.ID,
				UUID: dper.UUID,
				Name: dper.Name,
			})
		}
		resItem = append(resItem, myItem)

		allSummary = allSummary.Add(item.Price.Decimal)
	}

	for _, p := range resPerson {
		p.Summary = mapSummaryPerson[p.UUID]
	}
	resBill.Items = resItem
	resBill.Person = resPerson
	resBill.Summary = allSummary
	return resBill
}
