package services

import (
	"context"
	"tuangnoi/connection"
	"tuangnoi/myconfig/myvar"
)

func (s *sService) SendLineMessage(ctx context.Context, toTarget string, msg string) error {
	sendmsg := connection.LineBodySendMessage{
		To: toTarget,
		Message: append([]connection.LineBodyMessage{},
			connection.LineBodyMessage{
				Type: string(connection.TYPE_MESSAGE),
				Text: msg,
			},
		),
	}
	err := connection.LineSendMessage(myvar.ENV_LINE_ACCESS_TOKEN, sendmsg)
	if err != nil {
		return err
	}
	return nil
}

func (s *sService) SendLineBubble(ctx context.Context, toTarget string, content map[string]interface{}) error {
	sendmsg := connection.LineBodySendBubble{
		To: toTarget,
		Message: append([]connection.LineBodyBubble{},
			connection.LineBodyBubble{
				Type:    string(connection.TYPE_FLEX),
				Content: content,
			},
		),
	}
	err := connection.LineSendMessage(myvar.ENV_LINE_ACCESS_TOKEN, sendmsg)
	if err != nil {
		return err
	}
	return nil
}
