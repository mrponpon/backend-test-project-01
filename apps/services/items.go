package services

import (
	"context"
	"tuangnoi/myconfig/models"
)

func (s *sService) ItemGetByBillID(ctx context.Context, billID int) (interface{}, error) {
	mydata, err := s.r.ItemGetByBillID(ctx, billID)
	return mydata, err
}

func (s *sService) ItemUpdate(ctx context.Context, myinput models.BodyItemUpdate) error {

	for _, item := range myinput.Items {
		if err := s.r.ItemUpdate(ctx, models.DBItems{
			Name:  item.ItemName,
			Price: item.Price,
		}); err != nil {
			return err
		}
	}
	return nil
}

func (s *sService) ItemAddToBill(ctx context.Context, myinput models.BodyItemAddToBill) error {

	var insertItem = []models.DBItems{}
	for _, item := range myinput.Items {
		insertItem = append(insertItem, models.DBItems{
			Name:   item.ItemName,
			Price:  item.Price,
			BillID: myinput.BillID,
		})
	}

	return s.r.ItemAddToBill(ctx, insertItem)
}
