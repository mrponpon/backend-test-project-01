package services

import (
	"context"
	"strings"
	"tuangnoi/myconfig/models"
)

func (s *sService) PersonAddToBill(ctx context.Context, myinput models.BodyPersonAddToBill) error {

	for _, p := range myinput.Person {
		if err := s.r.PersonAddToBill(ctx, models.DBBillingPerson{
			BillID: myinput.BillID,
			Status: string(models.StatusUnPaid),
			UUID:   p.UUID,
			Name:   p.Name,
		}); err != nil {
			return err
		}
	}
	return nil
}

func (s *sService) PersonBillUpdate(ctx context.Context, myinput models.BodyBillingPersonUpdate) error {

	if err := s.r.PersonUpdate(ctx, models.DBBillingPerson{
		ID:      myinput.ID,
		Status:  myinput.Status,
		UUID:    myinput.UUID,
		Name:    myinput.Name,
		Summary: myinput.Summary,
	}); err != nil {
		return err
	}

	return nil
}

func (s *sService) PersonAddToItem(ctx context.Context, item models.BodyPersonAddToItem) error {

	var addPerson string

	for index, p := range item.Person {
		addPerson += p.PersonUUID
		if index != len(item.Person) {
			addPerson += ","
		}
	}

	if err := s.r.ItemUpdate(ctx, models.DBItems{
		ID:     item.ItemID,
		Person: addPerson,
	}); err != nil {
		return err
	}

	return nil
}

func (s *sService) PersonRemove(ctx context.Context, personID int) error {
	personData, err := s.r.PersonGetByID(ctx, personID)
	if err != nil {
		return err
	}

	if personData.ID == 0 {
		return nil
	}

	allItemOfPerson, err := s.r.ItemGetByBillID(ctx, personData.BillID)
	if err != nil {
		return err
	}

	for _, item := range allItemOfPerson {
		personOfItem := strings.Split(item.Person, ",")
		newDataPerson := s.removePersonInItem(personOfItem, personData.UUID)
		item.Person = strings.Join(newDataPerson, ",")
		if err := s.r.ItemUpdate(ctx, item); err != nil {
			return err
		}
	}

	if err := s.r.PersonRemove(ctx, personData.ID); err != nil {
		return err
	}

	return nil
}

func (s *sService) removePersonInItem(oldData []string, target string) []string {
	var newData []string
	for _, uuid := range oldData {
		if uuid != target {
			newData = append(newData, uuid)
		}
	}
	return newData
}
