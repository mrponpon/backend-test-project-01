package repositories

import (
	"context"
	"tuangnoi/myconfig/models"
	"tuangnoi/utils/helpers"
)

func (r *rRepo) PersonGetByBillID(ctx context.Context, billID int) ([]models.DBBillingPerson, error) {
	var mydata []models.DBBillingPerson
	if err := r.DB.Table(models.DBBillingPerson.TableName(models.DBBillingPerson{})).
		Where("bill_id=?", billID).
		Scan(&mydata).Error; err != nil {
		return mydata, helpers.MyErrFormat(err)
	}
	return mydata, nil
}

func (r *rRepo) PersonGetByID(ctx context.Context, personID int) (models.DBBillingPerson, error) {
	var mydata models.DBBillingPerson
	if err := r.DB.Table(models.DBBillingPerson.TableName(models.DBBillingPerson{})).
		Where("id=?", personID).Scan(&mydata).Error; err != nil {
		return mydata, helpers.MyErrFormat(err)
	}
	return mydata, nil
}

func (r *rRepo) PersonAddToBill(ctx context.Context, myinput models.DBBillingPerson) error {
	if err := r.DB.Table(models.DBBillingPerson.TableName(models.DBBillingPerson{})).
		Create(&myinput).Error; err != nil {
		return helpers.MyErrFormat(err)
	}
	return nil
}

func (r *rRepo) PersonUpdate(ctx context.Context, myinput models.DBBillingPerson) error {
	if err := r.DB.Table(models.DBBillingPerson.TableName(models.DBBillingPerson{})).
		Updates(&myinput).Error; err != nil {
		return helpers.MyErrFormat(err)
	}
	return nil
}

func (r *rRepo) PersonRemove(ctx context.Context, personID int) error {
	if err := r.DB.Table(models.DBBillingPerson.TableName(models.DBBillingPerson{})).
		Delete(&models.DBBillingPerson{}, personID).Error; err != nil {
		return helpers.MyErrFormat(err)
	}
	return nil
}
