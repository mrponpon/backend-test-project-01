package repositories

import (
	"context"
	"tuangnoi/myconfig/models"
	"tuangnoi/utils/helpers"
)

func (r *rRepo) ItemGetByBillID(ctx context.Context, billID int) ([]models.DBItems, error) {
	var mydata []models.DBItems
	if err := r.DB.Table(models.DBItems.TableName(models.DBItems{})).
		Where("bill_id=?", billID).Scan(&mydata).Error; err != nil {
		return nil, helpers.MyErrFormat(err)
	}
	return mydata, nil
}

func (r *rRepo) ItemAddToBill(ctx context.Context, myinput []models.DBItems) error {
	err := r.DB.Table(models.DBItems.TableName(models.DBItems{})).Create(&myinput).Error
	if err != nil {
		return helpers.MyErrFormat(err)
	}
	return nil
}

func (r *rRepo) ItemUpdate(ctx context.Context, myinput models.DBItems) error {
	err := r.DB.Table(models.DBItems.TableName(models.DBItems{})).Updates(&myinput).Error
	if err != nil {
		return helpers.MyErrFormat(err)
	}
	return nil
}
