package repositories

import (
	"context"
	"tuangnoi/myconfig/models"

	"gorm.io/gorm"
)

type rRepo struct {
	DB *gorm.DB
}

func NewRepo(DB *gorm.DB) Repositories {
	return &rRepo{
		DB: DB,
	}
}

type Repositories interface {
	BeginTransaction(ctx context.Context) *gorm.DB

	BillingGetByBillID(ctx context.Context, billID int) (models.DBBilling, error)
	BillingGetByPersonUUID(ctx context.Context, personUUID string) ([]models.DBBilling, error)
	BillingCreate(ctx context.Context, insertData models.DBBilling) (models.DBBilling, error)
	BillingUpdate(ctx context.Context, myinput models.DBBilling) error

	ItemGetByBillID(ctx context.Context, billID int) ([]models.DBItems, error)
	ItemAddToBill(ctx context.Context, myinput []models.DBItems) error
	ItemUpdate(ctx context.Context, myinput models.DBItems) error

	PersonAddToBill(ctx context.Context, myinput models.DBBillingPerson) error
	PersonUpdate(ctx context.Context, myinput models.DBBillingPerson) error
	PersonRemove(ctx context.Context, personID int) error
	PersonGetByBillID(ctx context.Context, billID int) ([]models.DBBillingPerson, error)
	PersonGetByID(ctx context.Context, personID int) (models.DBBillingPerson, error)
}
