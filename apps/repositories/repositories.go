package repositories

import (
	"context"

	"gorm.io/gorm"
)

func (r *rRepo) BeginTransaction(ctx context.Context) *gorm.DB {
	tx := r.DB.Begin()
	return tx
}
