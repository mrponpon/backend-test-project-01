package repositories

import (
	"context"
	"tuangnoi/myconfig/models"
	"tuangnoi/utils/helpers"
)

func (r *rRepo) BillingGetAll(ctx context.Context) ([]models.DBBilling, error) {
	var mydata []models.DBBilling
	err := r.DB.Table(models.DBBilling.TableName(models.DBBilling{})).Scan(&mydata).Error
	if err != nil {
		return mydata, helpers.MyErrFormat(err)
	}
	return mydata, nil
}

func (r *rRepo) BillingGetByBillID(ctx context.Context, billID int) (models.DBBilling, error) {
	var mydata models.DBBilling
	err := r.DB.Table(models.DBBilling.TableName(models.DBBilling{})).Where("id=?", billID).Scan(&mydata).Error
	if err != nil {
		return mydata, helpers.MyErrFormat(err)
	}
	return mydata, nil
}

func (r *rRepo) BillingGetByPersonUUID(ctx context.Context, personUUID string) ([]models.DBBilling, error) {

	qString := `
		select b.*
		from billing b
		join billing_person bp on bp.bill_id = b.id
		where bp.uuid = ?
	`

	var mydata []models.DBBilling
	err := r.DB.Raw(qString, personUUID).Scan(&mydata).Error
	if err != nil {
		return nil, helpers.MyErrFormat(err)
	}
	return mydata, nil
}

func (r *rRepo) BillingCreate(ctx context.Context, insertData models.DBBilling) (models.DBBilling, error) {
	err := r.DB.Table(models.DBBilling.TableName(models.DBBilling{})).Create(&insertData).Error
	if err != nil {
		return insertData, helpers.MyErrFormat(err)
	}
	return insertData, nil
}

func (r *rRepo) BillingUpdate(ctx context.Context, myinput models.DBBilling) error {
	err := r.DB.Table(
		models.DBBilling.TableName(models.DBBilling{})).
		Updates(myinput).Error
	if err != nil {
		return helpers.MyErrFormat(err)
	}
	return nil
}
