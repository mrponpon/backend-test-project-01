package handlers

import (
	"encoding/json"
	"strconv"
	"tuangnoi/myconfig/models"
	utilsModels "tuangnoi/utils/models"

	"github.com/gofiber/fiber/v2"
)

func (h *hanlers) ItemAddToBill(c *fiber.Ctx) error {

	var myinput models.BodyItemAddToBill
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}

	if err := h.s.ItemAddToBill(c.Context(), myinput); err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Create data success."))
}

func (h *hanlers) ItemGetByBillID(c *fiber.Ctx) error {
	input := c.Query("bill_id")

	billID, err := strconv.Atoi(input)
	if err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}

	items, err := h.s.ItemGetByBillID(c.Context(), billID)
	if err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Get data success.", items))
}

func (h *hanlers) ItemUpdate(c *fiber.Ctx) error {
	var myinput models.BodyItemUpdate
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}
	if err := h.s.ItemUpdate(c.Context(), myinput); err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Update data success."))
}
