package handlers

import (
	"github.com/gofiber/fiber/v2"
)

type Handlers interface {
	GetAll(c *fiber.Ctx) error
	WebhookCalback(c *fiber.Ctx) error
	PostLineMessage(c *fiber.Ctx) error
	PostLineBubble(c *fiber.Ctx) error

	BillingGetByID(c *fiber.Ctx) error
	BillingGetByPersonUUID(c *fiber.Ctx) error
	BillingCreate(c *fiber.Ctx) error
	BillingUpdate(c *fiber.Ctx) error

	ItemGetByBillID(c *fiber.Ctx) error
	ItemAddToBill(c *fiber.Ctx) error
	ItemUpdate(c *fiber.Ctx) error

	PersonAddToBill(c *fiber.Ctx) error
	PersonAddToItem(c *fiber.Ctx) error
	PersonBillUpdate(c *fiber.Ctx) error
	PersonRemove(c *fiber.Ctx) error
}
