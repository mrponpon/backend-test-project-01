package handlers

import (
	"encoding/json"
	"strconv"
	"tuangnoi/myconfig/models"
	utilsModels "tuangnoi/utils/models"

	"github.com/gofiber/fiber/v2"
)

func (h *hanlers) BillingGetAll(c *fiber.Ctx) error {
	return nil
}

func (h *hanlers) BillingGetByID(c *fiber.Ctx) error {
	input := c.Query("bill_id")
	billID, err := strconv.Atoi(input)
	if err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}
	mydata, err := h.s.BillingGetByID(c.Context(), billID)
	if err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Get data success.", mydata))
}

func (h *hanlers) BillingCreate(c *fiber.Ctx) error {
	var myinput models.BodyBillingCreate
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}

	return c.Status(200).JSON(utilsModels.ResponseSuccess("Create data success."))
}

func (h *hanlers) BillingUpdate(c *fiber.Ctx) error {
	var myinput models.DBBilling
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}

	if err := h.s.BillingUpdate(c.Context(), myinput); err != nil {
		return err
	}

	return c.Status(200).JSON(utilsModels.ResponseSuccess("Update data success."))
}

func (h *hanlers) BillingGetByPersonUUID(c *fiber.Ctx) error {
	personUUID := c.Query("person_uuid")
	mydata, err := h.s.BillingGetByPersonUUID(c.Context(), personUUID)
	if err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Get data success.", mydata))
}
