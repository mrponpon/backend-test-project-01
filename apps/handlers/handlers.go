package handlers

import (
	"encoding/json"
	"tuangnoi/apps/services"
	"tuangnoi/myconfig/models"
	utilsModels "tuangnoi/utils/models"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
)

type hanlers struct {
	s services.Services
}

func NewHandler(s services.Services) Handlers {
	return &hanlers{
		s: s,
	}
}

func (h *hanlers) GetAll(c *fiber.Ctx) error {

	return c.Status(200).JSON(utilsModels.ResponseSuccess("Get data success.", nil))
}

func (h *hanlers) WebhookCalback(c *fiber.Ctx) error {
	getbody := c.Body()
	log.Info().Msg("[WebhookCalback]:" + string(getbody))
	return c.Status(200).JSON(utilsModels.ResponseSuccess("success."))
}

func (h *hanlers) PostLineMessage(c *fiber.Ctx) error {
	var mybody = models.BodyLineMessage{}
	getbody := c.Body()
	err := json.Unmarshal(getbody, &mybody)
	if err != nil {
		return err
	}
	err = h.s.SendLineMessage(c.Context(), mybody.Target, mybody.Message)
	if err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Create data success."))
}

func (h *hanlers) PostLineBubble(c *fiber.Ctx) error {
	var mybody = models.BodyLineBubble{}
	getbody := c.Body()
	err := json.Unmarshal(getbody, &mybody)
	if err != nil {
		return err
	}
	err = h.s.SendLineBubble(c.Context(), mybody.Target, mybody.Content)
	if err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Create data success."))
}
