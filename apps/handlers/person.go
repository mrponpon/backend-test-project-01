package handlers

import (
	"encoding/json"
	"strconv"
	"tuangnoi/myconfig/models"
	utilsModels "tuangnoi/utils/models"

	"github.com/gofiber/fiber/v2"
)

func (h *hanlers) PersonAddToBill(c *fiber.Ctx) error {
	var myinput models.BodyPersonAddToBill
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}

	if err := h.s.PersonAddToBill(c.Context(), myinput); err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Add person success."))
}

func (h *hanlers) PersonBillUpdate(c *fiber.Ctx) error {
	var myinput models.BodyBillingPersonUpdate
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}

	if err := h.s.PersonBillUpdate(c.Context(), myinput); err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Update person success."))
}

func (h *hanlers) PersonAddToItem(c *fiber.Ctx) error {
	var myinput models.BodyPersonAddToItem
	if err := json.Unmarshal(c.Body(), &myinput); err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}
	if err := h.s.PersonAddToItem(c.Context(), myinput); err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Add person to item success."))
}

func (h *hanlers) PersonRemove(c *fiber.Ctx) error {
	rawPersonID := c.Params("person_id")
	personID, err := strconv.Atoi(rawPersonID)
	if err != nil {
		return c.Status(400).JSON(utilsModels.ResponseFail("Invalid input."))
	}
	if err := h.s.PersonRemove(c.Context(), personID); err != nil {
		return err
	}
	return c.Status(200).JSON(utilsModels.ResponseSuccess("Remove data success."))
}
