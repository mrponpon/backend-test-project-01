FROM golang:1.18-alpine

WORKDIR /marketplace

COPY go.mod /marketplace
COPY go.sum /marketplace
RUN go mod download

COPY src /marketplace/apps/backend/src
COPY libs /marketplace/libs


EXPOSE 8080

RUN cp /marketplace/apps/backend/src/main.go . && go build -o /marketapi
CMD [ "/marketapi" ]

